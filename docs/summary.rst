API Summary
===========

This contains a summary of the "public" API exposed by the project. Click on
each object for full API definitions.

The public classes are:

.. automodsumm:: tkmilan
   :classes-only:
   :nosignatures:
   :skip: Path, PackageNotFoundError, Binding, defaultdict

The public functions are:

.. automodsumm:: tkmilan
   :functions-only:
   :nosignatures:
   :skip: contextmanager, dataclass, lru_cache, metadata_version

The public variables are:

.. automodsumm:: tkmilan
   :variables-only:
   :nosignatures:
   :skip: logger, logger_eventbus, logger_image, logger_model_layout, logger_styling

This results in the following class diagram:

.. automod-diagram:: tkmilan
   :private-bases:
