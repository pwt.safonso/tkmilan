.. only:: (format_html or format_latex)

    .. image:: /_images/logo_horizontal.png
        :alt: Powertools Technologies
        :height: 75px

tkmilan
=======

This is documentation for the tkmilan module.

See also ancillary documentation:

- Upstream ``Tk`` :tk:`commands online manpages <contents.htm>`.
- Upstream ``Tk`` :tk_lib:`C library online manpages <contents.htm>`.
- New Mexico Tech ``tkinter`` :tkinter_nmt:`reference guide <index.html>`, by
  John W. Shipman.
  The `original location
  <http://www.nmt.edu/tcc/help/pubs/tkinter/web/index.html>`__ is offline, all
  links point to the latest mirror.
- effbot ``tkinter`` :tkinter_effbot:`introduction <>` by Fredrik Lundh. The
  `original location <https://effbot.org/tkinterbook/>`__ is offline, all links
  point to the latest mirror.

.. todo:: Include a hero image here, a screenshot of the showcase.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :name: topindex

   development

   summary
   api

Other Pages
-----------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. todo:: Include these links on the regular TOC

Metadata
--------

Current Version: |version|

.. only:: debug

   Current Release: |release|

Built |today|
