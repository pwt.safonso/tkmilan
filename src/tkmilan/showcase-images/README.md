# Images

Include here images to be used on the `tkmilan-showcase` entrypoint.

This is technically optional, it's not used anywhere else on `tkmilan`.
