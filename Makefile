# Settings
# - "rubber" is the target that build PDF files
IMPORTANT_DOCS := html
DOCSOPTS :=
RELEASEOPTS :=
TEST_PYTHON_VERSIONS := 3.8 3.9 3.10 3.11

# Build
# - Default: Build without isolation, for perfomance reasons
BUILD_OPTS := --no-isolation
# Sphinx
# - For production builds, add to SPHINXOPTS: `-t production`
# - To expose the HTML with an absolute URL, define the `BASEURL` environment variable
# - Languages: https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language
SPHINXOPTS :=
SPHINX_SOURCEDIR := docs
SPHINX_BUILDDIR := dist/docs
SPHINX_LANGUAGE := en
# Office Files
# - Development Location
OFFICE_BUILDDIR := $(SPHINX_BUILDDIR)/offices

# Define a specific Python version
# Define a specific Python architecture ("32"/"64" bits) (only on Windows)
# - Empty means no preference
# - Sync with .gitlab-ci.yml
PYTHON_VERSION := 3.8
PYTHON_BITS :=
# Configure Makefile.venv
PY=python$(PYTHON_VERSION)
REQUIREMENTS_TXT=requirements-dev.txt
PYTHON_VSTRING := $(shell $(PY) release/python-version.py)
VENVDIR?=$(WORKDIR)/.venv/$(PYTHON_VSTRING)
# https://github.com/sio/Makefile.venv
# - Version: v2022.07.20
include release/Makefile.venv
# Makefile.virtualatex
# - Includes the dependencies for the configured Sphinx version
VLATEX_REQUIREMENTS=tlmgr.docs.txt
include release/Makefile.virtualatex
# Virtual Environment Helpers
.PHONY: show-venv-bin venv-update
show-venv-bin: | venv
	@echo $(VENV)
venv-update:
	# Reload the venv @ "$(VENVDIR)"
	@# - Upgrade all Python depedencies
	@$(MAKE) -B venv PIP_UPGRADE=yes
# Let child scripts use the $VENV variable
export VENV
# Configure Project Name
# TODO: `$(PY) setup.py --name` prints spurious error messages
PROJECT := tkmilan
export PROJECT
# Support coverage Python wrapper
COVERAGE_WRAPPER := release/test-python-coverage
# Configure CI
VENV_WHEELHOUSE := $(WORKDIR)/.venv/wheelhouse
VENV_WHEELHOUSE_LIST := $(WORKDIR)/.venv/wheelhouse-linux.txt
include release/setup-cache.make
# Default Goal: To Do when running `make`
.DEFAULT_GOAL := dev-setup

.PHONY: dev-setup dev-wheelhouse venv-wheelhouse
dev-setup: | venv
dev-wheelhouse: | venv
	# Create the wheelhouse, based on the current environment
	mkdir -p $(VENV_WHEELHOUSE)
	$(VENV)/python release/dev-wheelhouse.py >$(VENV_WHEELHOUSE_LIST)
	$(VENV)/pip wheel -r $(VENV_WHEELHOUSE_LIST) --wheel-dir $(VENV_WHEELHOUSE)
venv-wheelhouse: $(VENV_WHEELHOUSE_LIST) | $(VENV)
	# Same as "make venv", but using the wheelhouse
	$(VENV)/pip install --no-index -f $(VENV_WHEELHOUSE) -r $<
	$(VENV)/pip install -e .
	@echo "wheelhouse + ." >$(VENV)/$(MARKER)

.PHONY: docs docs-all docs-generate docs-intersphinx docs-office doc-rubber
docs: docs-all # Debug, not for production
	release/link-docs "$(SPHINX_BUILDDIR)"
docs-all: $(IMPORTANT_DOCS:%=doc-%) docs-office
# Sphinx Setup
# - Generate PDF
doc-rubber: doc-latex | venv vlatex
	release/rubber-at "$(SPHINX_BUILDDIR)/latex" -m xelatex "$(PROJECT).tex"
doc-%: $(SPHINX_SOURCEDIR)/conf.py docs-generate docs-intersphinx | venv
	mkdir -p "$(SPHINX_SOURCEDIR)/_static"
	# sphinx-build -M ${@:doc-%=%} -l $(SPHINX_LANGUAGE) -- $(SPHINXOPTS)
	@$(VENV)/sphinx-build -M ${@:doc-%=%} "$(SPHINX_SOURCEDIR)" "$(SPHINX_BUILDDIR)" -D"language=$(SPHINX_LANGUAGE)" $(SPHINXOPTS)
docs-generate: | venv
	$(RM) $(wildcard $(SPHINX_SOURCEDIR)/api/*.rst)
	$(VENV)/sphinx-autogen "$(SPHINX_SOURCEDIR)/api.rst" -o "$(SPHINX_SOURCEDIR)/api/"
docs-intersphinx: | venv
	$(VENV)/python release/docs-intersphinx.py --timeout 5 --retry 3 "$(SPHINX_SOURCEDIR)/_inventories"
# Office Files
docs-office:
	mkdir -p "$(OFFICE_BUILDDIR)"
	release/list-docs-office -q -0 | xargs -0 --max-args=1 --no-run-if-empty -- release/build-doc-office -F "$(OFFICE_BUILDDIR)"

# Development Setup
bash: dev-completion

.PHONY: dev-completion dev-manpages
dev-completion: | venv
	@# Run `make dev-completion` to re-create the completions
	@release/setup-completion -o
	# bash|tcsh: Run `source release/completions.(bash|tcsh)` for shell completion
dev-manpages: | venv
	# Run `make doc-man` to create the manpages

.PHONY: build build-python build-common
build: build-python | venv

build-python: | venv
	# Python build process
	$(VENV)/python -m build --sdist --wheel


# Tests
TEST_LOCATIONS__SRC := src/
TEST_LOCATIONS__TST := tests/
LINT_LOCATIONS := $(TEST_LOCATIONS__SRC) $(TEST_LOCATIONS__TST)
.PHONY: test test-all
test: test-flake8 test-mypy test-unittest
test-all: test test-docs
.PHONY: test-flake8 test-ruff test-mypy test-unittest
test-flake8: | venv
	@# TODO: Generate JUnit XML report and HTML report @ dist/tests/flake8/
	$(VENV)/flake8 $(LINT_LOCATIONS)
test-ruff: $(VENV)/ruff | venv
	# FUTURE: Alternative to "flake8"
	@# TODO: When migrated, move to `requirements-dev.txt` and remove `$(VENV)/ruff` above
	$(VENV)/ruff check -- $(LINT_LOCATIONS)
test-mypy: | venv
	@# TODO: Generate JUnit XML report and HTML report @ dist/tests/mypy/
	$(VENV)/mypy $(TEST_LOCATIONS__SRC)
test-unittest: | venv
	@# TODO: Use coverage, create an HTML report @ dist/tests/coverage/
	$(VENV)/python -m unittest discover -v --start-directory $(TEST_LOCATIONS__TST)
test-docs: doc-linkcheck

# Release
.PHONY: release-test release-build release-pypi
release-test: $(foreach pv,$(TEST_PYTHON_VERSIONS),release-test--$(pv))
	# Tested other versions: $(TEST_PYTHON_VERSIONS)
release-test--%:
	$(MAKE) -j test PYTHON_VERSION=$*
release-build: docs-generate | venv
	$(VENV)/python -m build $(BUILD_OPTS)
release-pypi: | venv
	release/pypi-build
	# To publish, use:
	# - interactive choices: release/pypi-publish-interactive
	# - GitLab Repository: release/pypi-publish-gitlab
	
.PHONY: postprocess-debug
postprocess-debug: | venv
	release/postprocess-debug

.PHONY: clean-all
clean-all: clean clean-docs-inventories clean-git clean-pypi clean-vlatex clean-venv
.PHONY: clean clean-docs clean-build clean-test clean-docs-inventories clean-release clean-git clean-pypi
clean: clean-build clean-docs clean-test

clean-docs:
	$(RM) -r "$(SPHINX_BUILDDIR)" "$(OFFICE_BUILDDIR)"
clean-build:
	$(RM) -r build/ release/completions/*.auto.bash release/completions/*.auto.tcsh
clean-docs-inventories:
	$(RM) -r "$(SPHINX_SOURCEDIR)/_inventories"
clean-release: | venv
	$(VENV)/python release/clean-release.py dist
clean-test:
	$(RM) -r "dist/tests"
clean-git:
	@echo "WARNING: This might delete important files, you have 5 seconds to cancel (use Ctrl-C)"
	@sleep 5
	@echo "Deleting..."
	git clean -f -dX
clean-pypi:
	$(RM) -r "dist/pypi"
