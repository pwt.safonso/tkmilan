#!/usr/bin/env python3
import sys
import argparse
import json
from pathlib import Path
from textwrap import dedent

import requests  # Sphinx dependency

python_vi = sys.version_info

# Map: from "key" to "URL Base"
MAPPING = {
    'python': 'https://docs.python.org/%d.%d' % (python_vi.major, python_vi.minor),
}


def main():
    parser = argparse.ArgumentParser(description=dedent('''\
        Download the intersphinx inventories and store them on the source directory.
        They rarely change, so it is best to not download this too often.
    '''))
    # parser.add_argument('SPHINX_SOURCEDIR', type=Path)
    parser.add_argument('inventories_folder', type=Path,
                        help='''The folder to download inventories.
                        Usually a subfolder of "$SPHINX_SOURCEDIR"''')
    parser.add_argument('--force', action='store_true',
                        help='Force download inventories, if they already exist')
    parser.add_argument('--timeout', type=int, default=5,
                        help='Request timeout, in seconds. Defaults to "%(default)s".')
    parser.add_argument('--retry', type=int, default=0,
                        help='Request retry quantities. Defaults to "%(default)s".')
    args = parser.parse_args()

    metadata_path = args.inventories_folder / 'metadata.json'

    args.inventories_folder.mkdir(exist_ok=True)

    rsession = requests.Session()
    radapter = requests.adapters.HTTPAdapter(max_retries=args.retry)
    rsession.mount('http://', radapter)
    rsession.mount('https://', radapter)

    for key, url_base in MAPPING.items():
        path = args.inventories_folder / f'{key}.inv'
        url = f'{url_base}/objects.inv'
        if args.force is False and path.exists():
            print(f'[intersphinx] Inventory "{key}": Exists, skipping')
        else:
            print(f'[intersphinx] Inventory "{key}": Download, from "{url}"')
            try:
                req = rsession.get(url, timeout=args.timeout)
                with path.open('wb') as pathfd:
                    for chunk in req.iter_content():
                        pathfd.write(chunk)
            except requests.exceptions.Timeout:
                print('[intersphinx] - ERROR: timeout')
                if path.exists():
                    path.unlink()
            except requests.exceptions.RequestException as e:
                print(f'[intersphinx] - ERROR: {e}')
    print(f'[intersphinx] Metadata: {metadata_path}')
    with metadata_path.open('w') as metadata_fobj:
        json.dump(
            MAPPING,
            metadata_fobj,
        )
    return 0  # Always good


if __name__ == '__main__':
    sys.exit(main())
