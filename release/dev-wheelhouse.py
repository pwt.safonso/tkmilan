#!/usr/bin/env python
import sys
import logging
import argparse
import os
import subprocess
import json

logger = logging.getLogger(__name__)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''Windows-Compatible "pip freeze".''')
    parser.add_argument('-v', '--verbose', dest='loglevel',
                        action='store_const', const=logging.DEBUG, default=logging.INFO,
                        help='Add more details to the standard error log')
    parser.add_argument('--this', action='store_true',
                        help='Include the current package on the list. Disabled by default')

    parser.add_argument('extra', nargs='*',
                        help='Passed to "pip".')
    args = parser.parse_args()

    # Logs
    logs_fmt = '%(levelname)-5.5s| %(message)s'
    try:
        import coloredlogs  # type: ignore
        coloredlogs.install(level=args.loglevel, fmt=logs_fmt)
    except ImportError:
        logging.basicConfig(level=args.loglevel, format=logs_fmt)
    logging.captureWarnings(True)

    # Same as "pip freeze --exclude $PROJECT", but UTF-8 safe
    pip_list_json = subprocess.Popen([
        sys.executable, '-m', 'pip',
        'list',
        '--format=json',
        '--exclude', os.environ['PROJECT'],
        *args.extra,
    ], stdout=subprocess.PIPE)
    pip_list = json.load(pip_list_json.stdout)

    for pkg_obj in pip_list:
        assert 'editable_project_location' not in pkg_obj, 'Editable Package: {pkg_obj["name"]}'
        print('%s==%s' % (pkg_obj['name'], pkg_obj['version']))
    if args.this:
        print('.')  # This package
