@echo off
call run-cli.cmd
REM Reimplements "make test-unittest"

python -m unittest discover --start-directory tests
:: TODO: Use "pytest" instead
:: TODO: Use "coverage" instead of "python"
REM set TEST_RESULTS=dist\tests
REM set TEST_RESULTS_COVERAGE=%TEST_RESULTS%\coverage\
:: set COVERAGE_FILE=%TEST_RESULTS_COVERAGE%\data.%COMPUTERNAME%.%NOW%
:: Read COVERAGE_FILE as sqlite3
:: On the "file" table, turn "path" into a PosixPath (it's already relative)
