@echo off
call run-cli.cmd
REM Reimplements "make test-flake8"

flake8 python/ tests/
REM TODO: Use "flake8-html" to generate a report
REM set TEST_RESULTS=dist\tests
REM set TEST_RESULTS_FLAKE8=%TEST_RESULTS%\flake8\
REM Args:: --format=html --htmldir=%TEST_RESULTS_FLAKE8%
REM TODO: Generate a JUnit XML file (useful for CI)
