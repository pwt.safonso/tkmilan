# Set the pip cache directory
# Set the PyInstaller cache directory (named CONFIG, has another subdirectory)

# - Use the Gitlab variable, when running on CI
ifneq ($(CI_PROJECT_DIR),)
PIP_CACHE_DIR := $(CI_PROJECT_DIR)/.venv/cache-python/pip
PYINSTALLER_CONFIG_DIR := $(CI_PROJECT_DIR)/.venv/cache-python
export PIP_CACHE_DIR PYINSTALLER_CONFIG_DIR
endif
# vim: ft=make
