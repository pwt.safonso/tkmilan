#!/bin/sh
set -e
PREFIX="${1:-/usr/local}"

# Make sure the "release-cli" binary is available
if command -v release-cli >/dev/null; then
	echo "# release-cli available"
else
	bin="$PREFIX/bin/release-cli"
	arch='amd64'  # TODO: Hardcoded to `amd64`
	version='latest'
	# Run the following command for the correct OS-architecture string
	# # go version | cut -f 4 -d ' ' | tr '/' '-'
	echo "# Installing release-cli version=$version arch=$arch"
	curl "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/$version/release-cli-linux-$arch" \
		-o "$bin"
	chmod +x "$bin"
fi
