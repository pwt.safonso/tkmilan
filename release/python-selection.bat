set PYTHON_VSTRING=
REM Read information from the Makefile
for /f %%i in ('py -3 release\makefile-parse.py Makefile -v PYTHON_VERSION') do set PYTHON_VERSION=%%i
for /f %%i in ('py -3 release\makefile-parse.py Makefile -v PYTHON_BITS') do set PYTHON_BITS=%%i
if "%PYTHON_BITS%" == "" (
	for /f %%i in ('py -%PYTHON_VERSION% release\python-version.py') do set PYTHON_VSTRING=%%i
) else (
	set PYTHON_VSTRING=%PYTHON_VERSION%-%PYTHON_BITS%
)
for /f "delims=" %%i in ('py -3 release\makefile-parse.py Makefile -v REQUIREMENTS_TXT') do set REQUIREMENTS_TXT=%%i
REM Windows Specific
set VENV_LOCATION=.venv\%PYTHON_VSTRING%

REM Detect errors
py -%PYTHON_VSTRING% -V
if %ERRORLEVEL% NEQ 0 (
	echo Python Version: %PYTHON_VERSION%[%PYTHON_BITS%]
	echo Python VSTRING: %PYTHON_VSTRING%
	REM Something went wrong, show the installed Python versions
	py -0
	exit /B 1
)
