if not exist %VENV_LOCATION% (
	echo - Creating Virtual Environment: %VENV_LOCATION%
	py -%PYTHON_VSTRING% -m venv %VENV_LOCATION%
)
REM Hide venv folder
attrib +H .venv /D
