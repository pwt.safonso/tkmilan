REM Usage: $0 "TITLE" "TEXT"
REM - Supports "\n" on TEXT, replaced by a true newline
DIM msgTitle, msgText
msgTitle = WScript.Arguments.Item(0)
msgText = Replace(WScript.Arguments.Item(1), "\n", vbNewLine)
REM https://ss64.com/vb/msgbox.html
WScript.Quit MsgBox(msgText, vbOKOnly+vbQuestion, msgTitle)
