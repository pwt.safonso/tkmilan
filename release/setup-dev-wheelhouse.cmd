@echo off
call release\python-selection.bat
if %ERRORLEVEL% NEQ 0 (
	exit /B %ERRORLEVEL%
)
REM Reimplements "make venv-wheelhouse"
REM See `Makefile` and `setup-dev.cmd`

set VENV_WHEELHOUSE=.venv\wheelhouse
set VENV_WHEELHOUSE_LIST=.venv\wheelhouse-windows.txt

call release\setup-venv.cmd

call release\activate-venv.cmd
call release\setup-dependencies.cmd
echo -- Install project dependencies (from wheelhouse)
python -m pip install --no-index -f %VENV_WHEELHOUSE% -r %VENV_WHEELHOUSE_LIST%
echo -- Install this project, development mode
pip install -e .
