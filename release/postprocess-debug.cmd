@echo off
call run-cli.cmd

for /f %%i in ('python setup.py --name') do set PROJECT_NAME=%%i
for /f %%i in ('python setup.py --version') do set RELEASE=%%i
echo # Project Name = %PROJECT_NAME%
echo # Release = %RELEASE%

set build=build
set dist=dist

REM This for the CI builds, to move them to the location
set built_cmd=%build%\bin-debug-post.cmd
python release\build-entrypoints generate-debugpost-python >%built_cmd%
echo # Debug: Python Entrypoints Post-Processing
call %built_cmd%
