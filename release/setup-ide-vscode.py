#!/usr/bin/env python3
import sys
import argparse
from pathlib import Path
import subprocess
import json


def relative_path(path: Path, workspace: Path) -> str:
    return str(path.resolve()).replace(str(workspace.resolve()), '${workspaceFolder}')


# TODO: Configurable entrypoint arguments?
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--workspace', type=Path,
                        default=Path(),
                        help='Workspace folder. Defaults to "%(default)s"')
    parser.add_argument('--terminal', default='integratedTerminal',
                        help='Setting: Console. Defaults to "%(default)s"')
    parser.add_argument('-f', '--force', action='store_true',
                        help='Force file creation')
    args = parser.parse_args()

    workspace = args.workspace.resolve()
    vscode_folder = workspace / '.vscode'

    if not vscode_folder.is_dir() or args.force:
        print('# Configure IDE: VSCode')
        vscode_folder.mkdir(exist_ok=True)

        file_settings = vscode_folder / 'settings.json'
        file_launch = vscode_folder / 'launch.json'

        ppath = Path(sys.executable).resolve()
        assert ppath.is_file()
        assert ppath.parent.is_dir()
        eps = subprocess.check_output([
            'python',
            str(Path('release') / 'build-entrypoints'),
            '--quiet',
            'list-python',
        ], text=True).splitlines()

        obj_settings = {
            'python.defaultInterpreterPath': str(ppath.parent),
        }
        obj_launch = {
            'version': '0.2.0',
            'configurations': [],
        }
        for ep in eps:
            ep_path = Path('build') / 'bin' / ep
            obj_launch['configurations'].append({
                'name': f'Python Entrypoint: {ep}',
                'type': 'python',
                'request': 'launch',
                'justMyCode': True,
                'console': args.terminal,
                'cwd': relative_path(workspace, workspace),
                'program': relative_path(ep_path.resolve(), workspace),
                'args': [  # Known-Good argments
                    '-h',
                ],
            })

        with file_settings.open('w') as fobj_settings:
            print(f'- Writing "{file_settings}"')
            json.dump(obj_settings, fobj_settings,
                      indent=2)
        with file_launch.open('w') as fobj_launch:
            print(f'- Writing "{file_launch}"')
            json.dump(obj_launch, fobj_launch,
                      indent=2)
