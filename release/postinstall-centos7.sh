#!/bin/bash
set -e

# Setup EPEL repository
# Setup SCL repository
yum -y install epel-release centos-release-scl
yum updateinfo -y

# Include basic development tools
yum -y groupinstall "Development Tools"

# Python
# - pip,setuptools,wheel
# - tkinter
# Python 3.6
yum -y install python3 python3-{pip,setuptools,wheel,tkinter}
# Python 3.8
yum install -y rh-python38 rh-python38-python-{pip,setuptools,wheel,tkinter}
ln -sv /opt/rh/rh-python38/enable /etc/profile.d/rh-python38.sh
