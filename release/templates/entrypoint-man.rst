hello
=====

Synopsis
--------

.. Do not show `--version` and `-h/--help` arguments on synopsis

::

   hello [-v]

Description
-----------

Prints Hello World.

Arguments
---------

.. Keep `--version` and `-h/--help` arguments on bottom
.. See https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-standard-domain

.. program:: hello

.. option:: -v, --verbose

   Add more details to the standard error log

.. option:: --version

   Show program's version number and exit

.. option:: -h, --help

   Show program help and exit

.. program:: None

Exit Status
-----------

Always returns ``0``.

Examples
--------

There are no examples to be given.

.. vim: et:sw=3:ts=3:
