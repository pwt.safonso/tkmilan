#!/bin/bash
# - Make sure the virtual environment is setup
make venv
# Activate the virtual environment
# shellcheck disable=1091
source "$(unset -v MAKELEVEL; make show-venv-bin)/activate"

# - Older tk version, get Pillow
# - Install missing mypy stubs
pip install pillow \
	types-Pillow
