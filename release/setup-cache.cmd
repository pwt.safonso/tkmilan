REM Set the pip cache directory
REM Set the PyInstaller cache directory (named CONFIG, has another subdirectory)

REM - Use the Gitlab variable, when running on CI
if defined CI_PROJECT_DIR (
	set PIP_CACHE_DIR=%CI_PROJECT_DIR%\.venv\cache-python\pip
	set PYINSTALLER_CONFIG_DIR=%CI_PROJECT_DIR%\.venv\cache-python
)
