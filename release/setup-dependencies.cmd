echo - Setup Dependencies
echo -- Update pip
python -m pip install -U pip
echo -- Install latest "wheel"
REM - Silence warnings
REM - Speed up the next installation
echo -- Install latest "setuptools"
pip install -U setuptools wheel
if exist release\unrequirements.txt (
	echo -- Remove old requirements
	pip uninstall --yes -r release\unrequirements.txt
)
