#!/usr/bin/env python3
import sys
import argparse
from pathlib import Path
from shutil import rmtree

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dist', type=Path,
                        help='"dist" folder')
    args = parser.parse_args()

    print('# Delete build folders')
    for spath in args.dist.iterdir():
        if spath.is_dir():
            print(f'- {spath}')
            rmtree(spath, ignore_errors=True)
    sys.exit(0)
