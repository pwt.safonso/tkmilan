# Set the pip cache directory
# Set the PyInstaller cache directory (named CONFIG, has another subdirectory)

# - Use the Gitlab variable, when running on CI
if [ -n "$CI_PROJECT_DIR" ]; then
	export PIP_CACHE_DIR="$CI_PROJECT_DIR/.venv/cache-python/pip"
	export PYINSTALLER_CONFIG_DIR="$CI_PROJECT_DIR/.venv/cache-python"
fi
