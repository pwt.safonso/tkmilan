#!/usr/bin/env python3
import argparse
import re

RE = re.compile(r'^(?P<var>\w+)\s*:?=\s*(?P<value>.*)$')


# Can be exposed as library
def parse(fobj):
    DICT = {}
    for raw_line in fobj:
        m = RE.match(raw_line.rstrip('\n'))
        if m:
            key = m.group('var')
            value = m.group('value')
            DICT[key] = value
    return DICT


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parse Makefile for information',
    )
    parser.add_argument('makefile', type=argparse.FileType('r'),
                        help='Makefile to parse')
    parser.add_argument('-v', '--variable', default=None,
                        help='Variable Name to extract')
    args = parser.parse_args()

    DICT = parse(args.makefile)
    if args.variable:
        print(DICT[args.variable])
    else:
        # Debug
        for key, value in DICT.items():
            print('%s: "%s"' % (key, value))
