if exist %VENV_LOCATION% (
	echo - Cleaning Virtual Environment: %VENV_LOCATION%
	RMDIR /S /Q %VENV_LOCATION%
)
