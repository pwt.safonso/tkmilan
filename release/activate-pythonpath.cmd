if "%PYTHONPATH%" == "" (
	echo # Setting PYTHONPATH to '%1'
	set PYTHONPATH=%~f1
) else (
	echo %PYTHONPATH% | findstr /C:"%~f1" /B >NUL
	if %ERRORLEVEL% == 0 (
		echo # Keep PYTHONPATH starting on '%1'
	) else (
		echo # Augment PYTHONPATH with '%1'
		set PYTHONPATH=%~f1;%PYTHONPATH%
	)
)
:: Include trailing separator
echo %PYTHONPATH% | findstr /C:";" /E >NUL
if NOT %ERRORLEVEL% == 0 (
	set PYTHONPATH=%PYTHONPATH%;
)
