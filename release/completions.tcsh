#!/bin/tcsh
echo "# Load tcsh completions" >>& /dev/stderr
# Use the current file path as root directory
foreach f (release/completions/*.tcsh)
	echo "# - $f" >>& /dev/stderr
	source "$f"
end
