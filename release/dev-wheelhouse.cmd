@echo off
call run-cli.cmd
if %ERRORLEVEL% NEQ 0 (
	exit /B %ERRORLEVEL%
)
REM Reimplements "make dev-wheelhouse"

REM See `Makefile`
set VENV_WHEELHOUSE=.venv\wheelhouse
set VENV_WHEELHOUSE_LIST=.venv\wheelhouse-windows.txt

echo WheelHouse List: %VENV_WHEELHOUSE_LIST%
python release\dev-wheelhouse.py -v >%VENV_WHEELHOUSE_LIST%

echo WheelHouse: %VENV_WHEELHOUSE%
python -m pip wheel -r %VENV_WHEELHOUSE_LIST% --wheel-dir %VENV_WHEELHOUSE%

