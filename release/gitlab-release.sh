#!/bin/bash
set -e
set -o pipefail

PROJECT_NAME="$(python3 setup.py --name)"
RELEASE="v$(python3 setup.py --version)"

# Assume this is running from the repository root
build="build"
dist="dist"

mkdir --parents "$build"

# Check for necessary variables
for var in CI_API_V4_URL CI_PROJECT_ID; do
	if [ ! -v "$var" ]; then
		echo "ERROR: Missing Gitlab Variable: $var"
		exit 1
	fi
done
# Check for necessary programs
for prog in curl release-cli; do
	if ! command -v "$prog" >/dev/null 2>/dev/null; then
		echo "ERROR: Missing '$prog'"
		exit 2
	fi
done
# Gitlab Authentication
curl_auth=()
if [ -v CI_JOB_TOKEN ]; then
	curl_auth=(--header "JOB-TOKEN: $CI_JOB_TOKEN")
elif [ -v GITLAB_PRIVATE_TOKEN ]; then
	curl_auth=(--header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN")
else
	echo "ERROR: Missing Gitlab Authenticattion: \$GITLAB_PRIVATE_TOKEN"
	exit 10
fi

# Pretty-Printer
jq=(python -m 'json.tool' --sort-keys)
if command -v jq >/dev/null 2>/dev/null; then
	# jq: JSON pretty-printer
	#    - Use only if available
	jq=(jq -S .)
fi

# Gitlab Generic Packages
base_url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/$PROJECT_NAME/$RELEASE"
declare -A artifacts
for file in "$dist"/*; do
	if [ -f "$file" ]; then
		fname="${file##*/}"
		echo "# $fname"
		echo "- File: $file"
		url="$base_url/$fname?select=package_file"
		artifacts["$fname"]="$base_url/$fname"
		echo "- URL: $url"
		echo "- Metadata::"
		curl --fail --no-progress-meter "${curl_auth[@]}" "$@" \
			--upload-file "$file" \
			"$url" | "${jq[@]}"
	fi
done

# Gitlab Release
# - Releases (NOT release candidates)
if ! [[ "$RELEASE" =~ rc ]]; then
	echo "# ${#artifacts[@]} artifacts"
	artifacts_opts=()
	for aname in "${!artifacts[@]}"; do
		aurl="${artifacts["$aname"]}"
		# Mark some packages files
		# - *setup*
		# - tarballs
		if [[ "$aname" =~ setup|\.tar\. ]]; then
			atype="package"
		else
			atype="other"
		fi
		artifacts_opts+=('--assets-link' '{"name": "'"$aname"'", "url": "'"$aurl"'", "link_type": "'"$atype"'"}')
	done
	release_description="build/description-$RELEASE.txt"
	# Include the tag description on the release
	# `--always` is technically unnecessary, since this should only run on proper tags
	# Convert the possible PGP signature to markdown verbatim mode
	git show --no-patch --format=format:'%b' "$(git describe --always)" |\
		tail -n+4 |\
		sed \
			-e 's@^-----BEGIN PGP SIGNATURE-----$@\n```\n\0@' \
			-e 's@^-----END PGP SIGNATURE-----$@\0\n```@' \
	>"$release_description"
	release-cli create \
		--name "$RELEASE" \
		--tag-name "$RELEASE" \
		--description "$release_description" \
		"${artifacts_opts[@]}"
fi

exit 0  # OK
