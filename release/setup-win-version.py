#!/usr/bin/env python
import os
import re
import sys
from pathlib import Path
from datetime import datetime
import typing
import logging
import argparse

from importlib.metadata import version as metadata_version, metadata
from versioneer import get_versions

logger = logging.getLogger(__name__)


def datetime_to_FILETIME(dt) -> int:
    # https://gist.github.com/Mostafa-Hamdy-Elgiar/9714475f1b3bc224ea063af81566d873
    if (dt.tzinfo is None) or (dt.tzinfo.utcoffset(dt) is None):
        dt = dt.replace(tzinfo=datetime.timezone.utc)
    # Nanoseconds since epoch
    # | January 1, 1970 as MS file time
    new = 116444736000000000 + int(dt.timestamp() * 10000000)
    return new


def generate_VSVersionInfo(project: str, entrypoint: str, hasOutput: bool,
                           ) -> typing.Optional[bytes]:
    metadata_project = metadata(project)
    metadata_versions = get_versions()

    logger.debug('# Available Metadata')
    for mid, mvalue in metadata_project.items():
        if '\n' in mvalue:
            logger.debug('>P> %s => ...' % (mid))
        else:
            logger.debug('>P> %s => "%s"' % (mid, mvalue))
    for vid, vvalue in metadata_versions.items():
        logger.debug('>V> %s => "%s"' % (vid, vvalue))

    # Date
    release_date = datetime.strptime(metadata_versions['date'], '%Y-%m-%dT%H:%M:%S%z')
    year = '%d' % release_date.year

    # Metadata information
    project_author = metadata_project.get('Author', 'Powertools Technologies')
    project_copyright = f'© {year}, {project_author}'
    project_description = metadata_project.get('Summary', f'{project} Description')
    project_version = metadata_version(project)
    project_url = metadata_project.get('Home-page', 'https://www.powertools-tech.com/')
    project_githash = metadata_versions['full-revisionid']
    project_is_dirty = metadata_versions['dirty']

    # Version Tuple
    vparsed = re.match(r'^v(?P<major>\d+)\.(?P<minor>\d+)(\.(?P<patch>\d+))?(?:rc(?P<rc>\d+))?', project_version)
    if vparsed:
        vmajor = int(vparsed.group('major'))
        vminor = int(vparsed.group('minor'))
        vpatch = int(vparsed.group('patch') or 0)  # Optional
        vrc = int(vparsed.group('rc') or 0)  # Optional
    else:
        vmajor, vminor, vpatch, vrc = None, None, None, None
    project_vtuple_file = (vmajor or 0, vminor or 0, vpatch or 0, vrc or 0)
    project_vtuple_product = (vmajor or 0, vminor or 0, vpatch or 0, 0)  # No RC
    project_is_release = vrc is not None and vrc == 0
    project_is_rc = vrc is not None and vrc != 0
    assert not (project_is_release and project_is_rc)

    # Derivate Strings
    project_releasename = f'{args.project}-{project_version}'

    # Project Information
    project_vinfo_comments = [
        f'Git: {project_githash}',
    ]
    if project_is_dirty:
        project_vinfo_comments.append('Private Build (uncommitted changes)')
    if project_is_release:
        project_vinfo_comments.append('Type: Release')
    elif project_is_rc:
        project_vinfo_comments.append('Type: Release Candidate')
    assert all('\n' not in s for s in project_vinfo_comments), 'Invalid Comment (Includes newlines)'
    project_vinfo = {
        'Comments': '; '.join(project_vinfo_comments),
        'CompanyName': project_author,
        'FileDescription': project_description,
        'FileVersion': '.'.join('%d' % n for n in project_vtuple_product),  # Type: version string (.-separated)
        'InternalName': f'{project_releasename}--{entrypoint}',
        'LegalCopyright': project_copyright,
        # 'LegalTrademarks': '',
        'OriginalFilename': project_releasename,
        **({
            'PrivateBuild': f'Created from dirty repository @ {project_githash}',
        } if project_is_dirty else {}),
        'ProductName': project,
        'ProductVersion': project_version,  # Type: str
        'Homepage': project_url,
        'Developer': 'Powertools Technologies',
    }

    # https://learn.microsoft.com/en-us/windows/win32/menurc/varfileinfo-block#remarks
    project_langid = 0x0  # Language Neutral
    project_charsetid = 1200  # Unicode

    # https://learn.microsoft.com/en-us/windows/win32/menurc/versioninfo-resource#remarks
    # https://github.com/MicrosoftDocs/sdk-api/blob/docs/sdk-api-src/content/verrsrc/ns-verrsrc-vs_fixedfileinfo.md#-field-dwfileflags
    project_fileflags = 0x0
    if project_is_dirty:
        project_fileflags |= 0x8  # VS_FF_PRIVATEBUILD
    if project_is_rc:
        project_fileflags |= 0x2  # VS_FF_PRERELEASE
    # https://github.com/MicrosoftDocs/sdk-api/blob/docs/sdk-api-src/content/verrsrc/ns-verrsrc-vs_fixedfileinfo.md#-field-dwfileos
    project_fileos = 0x00040000 | 0x00000004  # VOS_NT | VOS__WINDOWS32 (even though it's 64 bits)
    # https://github.com/MicrosoftDocs/sdk-api/blob/docs/sdk-api-src/content/verrsrc/ns-verrsrc-vs_fixedfileinfo.md#-field-dwfiletype
    project_filetype = 0x1  # VFT_APP
    # https://learn.microsoft.com/en-us/windows/win32/menurc/stringfileinfo-block
    project_langcharset = '%04X%04X' % (project_langid, project_charsetid)
    #
    project_date_64bit = datetime_to_FILETIME(release_date)
    project_date = ((project_date_64bit >> 32) & 0xFFFFFFFF, (project_date_64bit >> 0) & 0xFFFFFFFF)

    logger.info('# Metadata')
    logger.info('> VarFileInfo:Translation    :: (%x, %x)', project_langid, project_charsetid)
    logger.info('> FixedFileInfo.filevers     :: %s', project_vtuple_file)
    logger.info('> FixedFileInfo.prodvers     :: %s', project_vtuple_product)
    logger.info('> FixedFileInfo.flags        :: 0x%X', project_fileflags)
    logger.info('> FixedFileInfo.OS           :: 0x%X', project_fileos)
    logger.info('> FixedFileInfo.fileType     :: 0x%X', project_filetype)
    logger.info('> FixedFileInfo.date         :: "%s" 0x[%s %s]', release_date, *('{:04X}'.format(i) for i in project_date))
    logger.info('> StringFileInfo:lang-charset:: %s', project_langcharset)
    for k, v in project_vinfo.items():
        if '\n' in v:
            logger.info('- %s:' % k)
            for line in v.splitlines():
                logger.info('| %s' % line)
        else:
            logger.info('- %s: %s' % (k, v))

    if hasOutput and sys.platform == "win32":
        # Requires `pefile`, with requires Windows
        from PyInstaller.utils.win32 import versioninfo

        # https://learn.microsoft.com/en-us/windows/win32/menurc/vs-versioninfo
        project_vsversioninfo = versioninfo.VSVersionInfo(
            # https://github.com/pyinstaller/pyinstaller/blob/master/PyInstaller/utils/win32/versioninfo.py#L255-L278
            ffi=versioninfo.FixedFileInfo(
                filevers=project_vtuple_file,
                prodvers=project_vtuple_product,
                flags=project_fileflags,
                OS=project_fileos,
                fileType=project_filetype,
                date=project_date,
            ),
            kids=[
                versioninfo.VarFileInfo([
                    # TODO: Support multiple translations?
                    versioninfo.VarStruct('Translation', [project_langid, project_charsetid])
                ]),
                versioninfo.StringFileInfo([
                    versioninfo.StringTable(
                        project_langcharset,
                        [versioninfo.StringStruct(k, v) for k, v in project_vinfo.items()],
                    )
                ]),
            ],
        )

        logger.debug('# Version Info')
        return project_vsversioninfo
    elif hasOutput:
        logger.critical('Unsupported OS: %s', sys.platform)
        return None
    else:
        return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', dest='loglevel',
                        action='store_const', const=logging.DEBUG, default=logging.INFO,
                        help='Add more details to the standard error log')

    parser.add_argument('--project', default=os.environ.get('PROJECT'),
                        help='Project Name. Default "%(default)s".')
    parser.add_argument('--entrypoint',
                        help='Entrypoint Name')
    # TODO: Implement, control `project_langid`. Defaults to `SPHINX_LANGUAGE` on Makefile
    # parser.add_argument('--language',
    #                     help='Project Language')
    parser.add_argument('-o', '--output', type=Path, default=None,
                        help='Output Text VersionInfo Information')

    args = parser.parse_args()

    if args.project is None:
        parser.error('Missing Project Name')

    # Logs
    logs_fmt = '%(levelname)-5.5s %(name)s@%(funcName)s %(message)s'
    try:
        import coloredlogs  # type: ignore
        coloredlogs.install(level=args.loglevel, fmt=logs_fmt)
    except ImportError:
        logging.basicConfig(level=args.loglevel, format=logs_fmt)
    logging.captureWarnings(True)

    hasOutput = args.output is not None

    vsversioninfo = generate_VSVersionInfo(args.project, args.entrypoint, hasOutput,
                                           )

    if args.output:
        if vsversioninfo is None:
            return 1
        else:
            with args.output.open('w', encoding='UTF-8') as foutput:
                for line in str(vsversioninfo).splitlines():
                    logger.debug('| %s', line)
                    print(line, file=foutput)
            return 0
    else:
        return 0


if __name__ == '__main__':
    sys.exit(main())
